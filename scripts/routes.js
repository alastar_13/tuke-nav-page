import Mustache from "./mustache.js";

export default[
    {
        hash: 'login',
        target: 'router-view',
        getTemplate: generateHtml4Login
    },

    {
        hash: 'subjects',
        target: 'router-view',
        getTemplate: generateHtml4Subjects
    },

    {
        hash: 'signup',
        target: 'router-view',
        getTemplate: generateHtml4Signup
    }
];

function generateHtml4Login(targetElm){
    document.getElementById(targetElm).innerHTML = document.getElementById('template-login').innerHTML;
}

function generateHtml4Subjects(targetElm){
    document.getElementById(targetElm).innerHTML = document.getElementById('template-subjects').innerHTML;
}

function generateHtml4Signup(targetElm){
    document.getElementById(targetElm).innerHTML = document.getElementById('template-signup').innerHTML;
}