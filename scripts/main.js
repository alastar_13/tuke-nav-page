async function register(){
    const newUser = new Parse.Object('User');
    newUser.set('email', document.getElementById('emailSignup').value);
    newUser.set('password', document.getElementById('passwordSignup').value);
    try {
        const result = await newUser.save();
    } catch(error) {
        console.error(error);
    }
}